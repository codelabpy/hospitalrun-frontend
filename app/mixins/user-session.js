import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import Mixin from '@ember/object/mixin';
export default Mixin.create({
  session: service(),
  defaultCapabilities: {
    admin: [
      'Administrador de Usuario',
      'Administrador de Sistema',
      'Quality'
    ],
    add_allergy: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    appointments: [
      'Carga de Datos',
      'Cajero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_appointment: [
      'Carga de Datos',
      'Cajero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_charge: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_billing_diagnosis: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_diagnosis: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    add_medication: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    add_operative_plan: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    add_operation_report: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    add_photo: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_patient: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_pricing: [
      'Carga de Datos',
      'Cajero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_pricing_profile: [
      'Carga de Datos',
      'Cajero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_lab: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Lab Technician',
      'Administrador de Sistema'
    ],
    add_imaging: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_procmed: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_inventory_request: [
      'Carga de Datos',
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Enfermero',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    add_inventory_item: [
      'Carga de Datos',
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_inventory_purchase: [
      'Carga de Datos',
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_invoice: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_payment: [
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    add_procedure: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    add_socialwork: [
      'Hospital Administrator',
      'Medical Records Officer',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_user: [
      'Administrador de Usuario',
      'Administrador de Sistema'
    ],
    add_visit: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    add_vitals: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Administrador de Sistema'
    ],
    add_report: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Administrador de Sistema'
    ],
    admit_patient: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    adjust_inventory_location: [
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    billing: [
      'Hospital Administrator',
      'Cajero',
      'Finance Manager',
      'Administrador de Sistema'
    ],
    complete_imaging: [
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    complete_procmed: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    complete_lab: [
      'Lab Technician',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_appointment: [
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    delete_diagnosis: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    delete_inventory_item: [
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_imaging: [
      'Profesional',
      'Enfermero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_procmed: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_invoice: [
      'Hospital Administrator',
      'Administrador de Sistema'
    ],
    delete_lab: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_medication: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_photo: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    delete_patient: [
      'Hospital Administrator',
      'Medical Records Officer',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    delete_pricing: [
      'Cajero',
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_pricing_profile: [
      'Cajero',
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    delete_procedure: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    delete_socialwork: [
      'Hospital Administrator',
      'Medical Records Officer',
      'Social Worker',
      'Administrador de Sistema'
    ],
    delete_vitals: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Administrador de Sistema'
    ],
    delete_report: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Administrador de Sistema'
    ],
    delete_visit: [
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    delete_user: [
      'Administrador de Usuario',
      'Administrador de Sistema'
    ],
    discharge_patient: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    edit_invoice: [
      'Carga de Datos',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    fulfill_inventory: [
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    fulfill_medication: [
      'Medical Records Officer',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    imaging: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    procmed: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    invoices: [
      'Hospital Administrator',
      'Cajero',
      'Finance Manager',
      'Administrador de Sistema'
    ],
    labs: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Lab Technician',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    medication: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    inventory: [
      'Carga de Datos',
      'Hospital Administrator',
      'Inventory Manager',
      'Medical Records Officer',
      'Enfermero',
      'Pharmacist',
      'Administrador de Sistema'
    ],
    load_db: [
      'Administrador de Sistema'
    ],
    override_invoice: [
      'Hospital Administrator',
      'Administrador de Sistema'
    ],
    query_db: [
      'Administrador de Sistema'
    ],
    patients: [
      'Carga de Datos',
      'Profesional',
      'Cajero',
      'Finance Manager',
      'Hospital Administrator',
      'Imaging Technician',
      'Enfermero',
      'procmed Technician',
      'Lab Technician',
      'Medical Records Officer',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],

    patient_reports: [
      'Hospital Administrator',
      'Recepcionista',
      'Administrador de Sistema'
    ],

    pricing: [
      'Carga de Datos',
      'Cajero',
      'Hospital Administrator',
      'Medical Records Officer',
      'Administrador de Sistema'
    ],
    visits: [
      'Carga de Datos',
      'Profesional',
      'Hospital Administrator',
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Social Worker',
      'Administrador de Sistema'
    ],
    incident: [
      'Hospital Staff',
      'Administrador de Usuario',
      'Quality',
      'Administrador de Sistema'
    ],
    add_incident: [
      'Hospital Staff',
      'Administrador de Usuario',
      'Quality',
      'Administrador de Sistema'
    ],
    delete_incident: [
      'Quality',
      'Administrador de Sistema'
    ],
    generate_incident_report: [
      'Administrador de Usuario',
      'Quality',
      'Administrador de Sistema'
    ],
    add_incident_category: [
      'Administrador de Usuario',
      'Quality',
      'Administrador de Sistema'
    ],
    delete_incident_category: [
      'Quality',
      'Administrador de Sistema'
    ],
    manage_incidents: [
      'Quality',
      'Administrador de Sistema'
    ],
    update_config: [
      'Administrador de Sistema'
    ],
    users: [
      'Administrador de Usuario',
      'Administrador de Sistema',
      'Quality'
    ],
    add_note: [
      'Profesional',
      'Medical Records Officer',
      'Nurse',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    delete_note: [
      'Medical Records Officer',
      'Enfermero',
      'Recepcionista',
      'Administrador de Sistema'
    ],
    'define_user_roles': [
      'Administrador de Sistema'
    ]
  },

  _getUserSessionVars() {
    let session = this.get('session');
    if (!isEmpty(session) && session.get('isAuthenticated')) {
      return session.get('data.authenticated');
    }
  },

  currentUserRole() {
    let sessionVars = this._getUserSessionVars();
    if (!isEmpty(sessionVars) && !isEmpty(sessionVars.role)) {
      return sessionVars.role;
    }
    return null;
  },

  currentUserCan(capability) {
    let sessionVars = this._getUserSessionVars();
    if (!isEmpty(sessionVars) && !isEmpty(sessionVars.role)) {
      let userCaps = this.get('session').get('data.authenticated.userCaps');
      if (isEmpty(userCaps)) {
        let capabilities = this.get('defaultCapabilities');
        let supportedRoles = capabilities[capability];
        if (!isEmpty(supportedRoles)) {
          return supportedRoles.includes(sessionVars.role);
        }
      } else {
        return userCaps.includes(capability.camelize()); // User defined capabilities are camelcased.
      }
    }
    return false;
  },

  /**
   * Returns the display name of the user or the username if
   * the display name is not set or if the username is explictly requested.
   * @param {boolean} returnUserName if true, always return the username instead
   * of the display name even if the display name is set.
   */
  getUserName(returnUserName) {
    let returnName;
    let sessionVars = this._getUserSessionVars();
    if (!isEmpty(sessionVars)) {
      if (returnUserName) {
        returnName = sessionVars.name;
      } else if (!isEmpty(sessionVars.displayName)) {
        returnName = sessionVars.displayName;
      } else if (!isEmpty(sessionVars.name)) {
        returnName = sessionVars.name;
      }
    }
    return returnName;
  }
});
