import { translationMacro as t } from 'ember-i18n';
import Mixin from '@ember/object/mixin';
export default Mixin.create({
  canEdit: function() {
    let status = this.get('status');
    return (status === t('labels.requested'));
  }.property('status')
});
