import { map } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import SelectValues from 'hospitalrun/utils/select-values';
export default Mixin.create({
  appointmentStatusList: [
    'Atendido',
    'Agendada',
    'Cancelada',
    'Perdida'
  ],

  appointmentStatuses: map('appointmentStatusList', SelectValues.selectValuesMap),

  appointmentStatusesWithEmpty: function() {
    return SelectValues.selectValues(this.get('appointmentStatusList'), true);
  }.property(),

  appointmentTurnosList: [
    'Mañana',
    'Tarde'
  ],

  appointmentTurnos: map('appointmentTurnosList', SelectValues.selectValuesMap),

  appointmentTurnoWithEmpty: function() {
    return SelectValues.selectValues(this.get('appointmentTurnosList'), true);
  }.property()
});
