import PricingIndexRoute from 'hospitalrun/pricing/index/route';
export default PricingIndexRoute.extend({
  category: 'procmed',
  pageTitle: 'Procmed Pricing',

  actions: {
    editItem(item) {
      item.set('returnTo', 'pricing.procmed');
      this.transitionTo('pricing.edit', item);
    }
  }
});
