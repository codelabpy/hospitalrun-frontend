import { computed } from '@ember/object';
import AbstractModel from 'hospitalrun/models/abstract';
import CanEditRequested from 'hospitalrun/mixins/can-edit-requested';
import DateFormat from 'hospitalrun/mixins/date-format';
import DS from 'ember-data';
import PatientValidation from 'hospitalrun/utils/patient-validation';
import ResultValidation from 'hospitalrun/mixins/result-validation';

export default AbstractModel.extend(CanEditRequested, DateFormat, ResultValidation, {
  // Attributes
  medico: DS.attr('string'),
  notes: DS.attr('string'),
  procmedDate: DS.attr('date'),
  requestedBy: DS.attr('string'),
  requestedDate: DS.attr('date'),
  result: DS.attr('string'),
  status: DS.attr('string'),
  tipoProcedimiento: DS.attr('string'),

  // Associations
  charges: DS.hasMany('proc-charge', { async: false }),
  patient: DS.belongsTo('patient', { async: false }),
  procmedType: DS.belongsTo('pricing', { async: false }),
  visit: DS.belongsTo('visit', { async: false }),

  procmedDateAsTime: computed('procmedDate', function() {
    return this.dateToTime(this.get('procmedDate'));
  }),

  requestedDateAsTime: computed('requestedDate', function() {
    return this.dateToTime(this.get('requestedDate'));
  }),

  validations: {
    procmedTypeName: {
      presence: {
        'if'(object) {
          if (object.get('isNew')) {
            return true;
          }
        },
        message: 'Please select an procmed type'
      }
    },
    patientTypeAhead: PatientValidation.patientTypeAhead,
    patient: {
      presence: true
    }
  }
});