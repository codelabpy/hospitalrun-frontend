import AbstractModel from 'hospitalrun/models/abstract';
import DS from 'ember-data';

export default AbstractModel.extend({
  // Attributes
  dateRecorded: DS.attr('date'),
  dbp: DS.attr('number'),
  heartRate: DS.attr('number'),
  height: DS.attr('string'),
  respiratoryRate: DS.attr('number'),
  sbp: DS.attr('number'),
  temperature: DS.attr('number'),
  cefalica: DS.attr('string'),
  weight: DS.attr('string'),
  imc: DS.attr('string'),
  sat: DS.attr('string'),
  pulso: DS.attr('string')
});
