import { inject as service } from '@ember/service';
import { get } from '@ember/object';
import Route from '@ember/routing/route';
import MapOauthParams from 'hospitalrun/mixins/map-oauth-params';
import SetupUserRole from 'hospitalrun/mixins/setup-user-role';

export default Route.extend(MapOauthParams, SetupUserRole, {
  config: service(),
  database: service(),
  session: service(),
  model(params) {
    if (params.k && params.s1 && params.s2 && params.t) {
      get('session').authenticate('authenticator:custom', {
        google_auth: true,
        params
      });
      let oauthConfigs = {
        config_consumer_key: params.k,
        config_consumer_secret: params.s1,
        config_oauth_token: params.t,
        config_token_secret: params.s2
      };
      return get('config').saveOauthConfigs(oauthConfigs)
        .then(function() {
          oauthConfigs.config_use_google_auth = true;
          return get('database').setup(oauthConfigs).then(() => {
            return this.setupUserRole();
          });
        }.bind(this));
    }
  }
});
