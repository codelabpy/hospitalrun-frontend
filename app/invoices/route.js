import { translationMacro as t } from 'ember-i18n';
import AbstractModuleRoute from 'hospitalrun/routes/abstract-module-route';
import ModalHelper from 'hospitalrun/mixins/modal-helper';
import PatientListRoute from 'hospitalrun/mixins/patient-list-route';

export default AbstractModuleRoute.extend(ModalHelper, PatientListRoute, {
  addCapability: 'add_invoice',
  currentScreenTitle: 'Invoices',
  editTitle: t('invoices.titles.editTitle'),
  newTitle: t('invoices.titles.newTitle'),
  moduleName: 'invoices',
  newButtonText: t('navigation.subnav.newInvoice'),
  sectionTitle: 'Invoices',

  additionalButtons: function() {
    if (this.currentUserCan('add_payment')) {
      return [{
        class: 'btn btn-default',
        buttonText: t('invoices.buttons.plusAddNewDeposit'),
        buttonAction: 'showAddDeposit'
      }];
    }
  }.property(),

  additionalModels: [{
    name: 'billingCategoryList',
    findArgs: ['lookup', 'billing_categories']
  }, {
    name: 'expenseAccountList',
    findArgs: ['lookup', 'expense_account_list']
  }, {
    name: 'pricingProfiles',
    findArgs: ['price-profile']
  }],

  actions: {
    showAddDeposit() {
      let payment = this.store.createRecord('payment', {
        paymentType: 'Deposit',
        datePaid: new Date()
      });
      this.send('openModal', 'invoices.payment', payment);
    },

    showAddPayment(invoice) {
      let payment = this.store.createRecord('payment', {
        invoice,
        paymentType: 'Payment',
        datePaid: new Date()
      });
      this.send('openModal', 'invoices.payment', payment);
    },

    showEditPayment(payment) {
      if (this.currentUserCan('add_payment')) {
        this.send('openModal', 'invoices.payment', payment);
      }
    }
  },

  subActions: function() {
    let actions = [{
      text: t('invoices.subActions.billedText'),
      linkTo: 'invoices.index',
      statusQuery: 'Billed'
    }];
    if (this.currentUserCan('add_invoice')) {
      actions.push({
        text: t('invoices.subActions.draftText'),
        linkTo: 'invoices.index',
        statusQuery: 'Draft'
      });
      actions.push({
        text: t('invoices.subActions.all'),
        linkTo: 'invoices.index',
        statusQuery: 'All'
      });
    }
    if (this.currentUserCan('list_paid_invoices')) {
      actions.push({
        text: t('invoices.subActions.paid'),
        linkTo: 'invoices.index',
        statusQuery: 'Paid'
      });
    }
    return actions;
  }.property()

});
