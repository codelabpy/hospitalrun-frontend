import { alias } from '@ember/object/computed';
import { inject as controller } from '@ember/controller';
import { resolve } from 'rsvp';
import AbstractEditController from 'hospitalrun/controllers/abstract-edit-controller';

export default AbstractEditController.extend({
  cancelAction: 'closeModal',
  newCharge: false,
  requestingController: controller('imaging/edit'),
  medicationList: alias('requestingController.medicationList'),

  updateCapability: 'add_charge',

  title: function() {
    let isNew = this.get('model.isNew');
    if (isNew) {
      return this.get('i18n').t('imaging.titles.addMedicationUsed');
    }
    return this.get('i18n').t('imaging.titles.editMedicationUsed');
  }.property('model.isNew'),

  beforeUpdate() {
    let lista = document.getElementById('lista');
    let isNew = this.get('model.isNew');
    if (isNew) {
      this.set('newCharge', true);
      let model = this.get('model');
      let inventoryItem = model.get('inventoryItem');
      model.set('medication', inventoryItem);
      model.set('medicationTitle', inventoryItem.get('name'));
      model.set('priceOfMedication', inventoryItem.get('price'));
      model.set('medida', lista.value);
    }
    return resolve();
  },

  afterUpdate(record) {
    console.log('entro en el modal de after');
    if (this.get('newCharge')) {
      this.get('requestingController').send('addCharge', record);
    } else {
      this.send('closeModal');
    }
  }
});