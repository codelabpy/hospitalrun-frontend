import { resolve } from 'rsvp';
import { translationMacro as t } from 'ember-i18n';
import AbstractEditRoute from 'hospitalrun/routes/abstract-edit-route';
import AddToPatientRoute from 'hospitalrun/mixins/add-to-patient-route';
import ChargeRoute from 'hospitalrun/mixins/charge-route';
import moment from 'moment';
import PatientListRoute from 'hospitalrun/mixins/patient-list-route';
export default AbstractEditRoute.extend(AddToPatientRoute, ChargeRoute, PatientListRoute, {
  editTitle: t('imaging.titles.editTitle'),
  modelName: 'imaging',
  newTitle: t('imaging.titles.editTitle'),
  pricingCategory: 'Imaging',

  actions: {
    returnToAllItems() {
      this.controller.send('returnToAllItems');
    }
  },

  getNewData() {
    return resolve({
      selectPatient: true,
      requestDate: moment().startOf('day').toDate()
    });
  },

  setupController(controller, model) {
    this._super(controller, model);
    let medicationQuery = { key: 'Medication', include_docs: true };

    this.get('database').queryMainDB(medicationQuery, 'inventory_by_type').then((result) => {
      let medicationList = result.rows.map((medication) => {
        return medication.doc;
      });
      controller.set('medicationList', medicationList);
    });
  }
});
