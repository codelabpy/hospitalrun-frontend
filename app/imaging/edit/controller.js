import { alias } from '@ember/object/computed';
import { isArray } from '@ember/array';
import { isEmpty } from '@ember/utils';
import { inject as controller } from '@ember/controller';
import EmberObject from '@ember/object';
import AbstractEditController from 'hospitalrun/controllers/abstract-edit-controller';
import ChargeActions from 'hospitalrun/mixins/charge-actions';
import PatientSubmodule from 'hospitalrun/mixins/patient-submodule';

export default AbstractEditController.extend(ChargeActions, PatientSubmodule, {
  imagingController: controller('imaging'),

  chargePricingCategory: 'Imaging',
  chargeRoute: 'imaging.charge',
  selectedImagingType: null,
  medicationList: null,
  editController: controller('visits/edit'),

  canComplete: function() {
    let isNew = this.get('model.isNew');
    let imagingTypeName = this.get('model.imagingTypeName');
    let selectedImagingType = this.get('selectedImagingType');
    if (isNew && (isEmpty(imagingTypeName) || isArray(selectedImagingType) && selectedImagingType.length > 1)) {
      return false;
    } else {
      return this.currentUserCan('complete_imaging');
    }
  }.property('selectedImagingType.[]', 'model.imagingTypeName'),

  actions: {
    completeImaging() {
      this.set('model.status', 'Completado');
      this.get('model').validate().then(function() {
        if (this.get('model.isValid')) {
          this.set('model.imagingDate', new Date());
          this.send('update');
        }
      }.bind(this)).catch(function() {});
    },

    showAddMedication() {
      let newCharge = this.get('store').createRecord('proc-charge', {
        dateCharged: new Date(),
        newMedicationCharge: true,
        quantity: 1
      });
      this.send('openModal', 'imaging.medication', newCharge);
    },

    showEditMedication(charge) {
      let medicationList = this.get('medicationList');
      let selectedMedication = medicationList.findBy('id', charge.get('medication.id'));
      charge.set('itemName', selectedMedication.name);
      this.send('openModal', 'imaging.medication', charge);
    },

    showDeleteMedication(charge) {
      this.send('openModal', 'dialog', EmberObject.create({
        closeModalOnConfirm: false,
        confirmAction: 'deleteCharge',
        title: this.get('i18n').t('imagings.titles.deleteMedicationUsed'),
        message: this.get('i18n').t('imagings.messages.deleteMedication'),
        chargeToDelete: charge,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));
    },

    /**
     * Save the imaging request(s), creating multiples when user selects multiple imaging tests.
     */
    update() {
      // Nuevo
      // Si ya existia un registro de este paciente
      if (this.get('model.isNew')) {
        let newImaging = this.get('model');
        let selectedImagingType = this.get('selectedImagingType');
        if (isEmpty(this.get('model.status'))) {
          this.set('model.status', 'Solicitado');
        }
        this.set('model.requestedBy', newImaging.getUserName());
        this.set('model.requestedDate', new Date());
        if (isEmpty(selectedImagingType)) {
          this.saveNewPricing(this.get('model.imagingTypeName'), 'Imaging', 'model.imagingType').then(function() {
            this.addChildToVisit(newImaging, 'imaging', 'Procedimiento Enfermeria').then(function() {
              this.saveModel();
            }.bind(this));
          }.bind(this));

        } else {
          this.getSelectedPricing('selectedImagingType').then(function(pricingRecords) {
            if (isArray(pricingRecords)) {
              this.createMultipleRequests(pricingRecords, 'imagingType', 'imaging', 'Imaging');
            } else {
              this.set('model.imagingType', pricingRecords);
              this.addChildToVisit(newImaging, 'imaging', 'Procedimiento Enfermeria').then(function() {
                this.saveModel();
              }.bind(this));
            }
          }.bind(this));
        }

        // Edicion
      } else {
        if (isEmpty(this.get('model.status'))) {
          let newImaging = this.get('model');
          this.set('model.status', 'Solicitado');
          this.set('model.requestedBy', newImaging.getUserName());
          this.set('model.requestedDate', new Date());
          this.set('model.imagingDate', new Date());
          let selectedImagingType = this.get('selectedImagingType');
          if (isEmpty(selectedImagingType)) {
            this.saveNewPricing(this.get('model.imagingTypeName'), 'Imaging', 'model.imagingType').then(function() {
              this.addChildToVisit(newImaging, 'imaging', 'Imaging').then(function() {
                this.saveModel();
              }.bind(this));
            }.bind(this));
          } else {
            this.getSelectedPricing('selectedImagingType').then(function(pricingRecords) {
              if (isArray(pricingRecords)) {
                this.createMultipleRequests(pricingRecords, 'imagingType', 'imaging', 'Imaging');
              } else {
                this.set('model.imagingType', pricingRecords);
                this.addChildToVisit(newImaging, 'imaging', 'Imaging').then(function() {
                  this.saveModel();
                }.bind(this));
              }
            }.bind(this));
          }
        }
        this.saveModel();
      }
    }
  },

  additionalButtons: function() {
    let i18n = this.get('i18n');
    let canComplete = this.get('canComplete');
    let isValid = this.get('model.isValid');
    if (isValid && canComplete) {
      return [{
        buttonAction: 'completeImaging',
        buttonIcon: 'glyphicon glyphicon-ok',
        class: 'btn btn-primary on-white',
        buttonText: i18n.t('buttons.complete')
      }];
    }
  }.property('canComplete', 'model.isValid'),

  lookupListsToUpdate: [{
    name: 'enfermeroList',
    property: 'model.enfermero',
    id: 'enfermeros'
  }, {
    name: 'tipoProcedimientoEnfermeriaList',
    property: 'model.tipoProcedimientoEnfermeria',
    id: 'tipoProcedimientoEnfermerias'
  }],

  pricingTypeForObjectType: 'Imaging imaging',
  pricingTypes: alias('imagingController.imagingPricingTypes'),
  tipoProcedimientoEnfermeriaList: alias('imagingController.tipoProcedimientoEnfermeriaList'),

  pricingList: null, // This gets filled in by the route

  enfermeroList: alias('imagingController.enfermeroList'),

  updateCapability: 'add_imaging',

  afterUpdate(saveResponse, multipleRecords) {
    let i18n = this.get('i18n');
    this.updateLookupLists();
    let afterDialogAction,
      alertTitle,
      alertMessage;
    if (this.get('model.status') === 'Completado') {
      alertTitle = i18n.t('imaging.alerts.completedTitle');
      alertMessage = i18n.t('imaging.alerts.completedMessage');
    } else {
      alertTitle = i18n.t('imaging.alerts.savedTitle');
      alertMessage = i18n.t('imaging.alerts.savedMessage');
    }
    if (multipleRecords) {
      afterDialogAction = this.get('cancelAction');
    }
    this.saveVisitIfNeeded(alertTitle, alertMessage, afterDialogAction);
    this.set('model.selectPatient', false);
  }

});