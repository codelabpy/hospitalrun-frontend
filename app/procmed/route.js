import { translationMacro as t } from 'ember-i18n';
import AbstractModuleRoute from 'hospitalrun/routes/abstract-module-route';
export default AbstractModuleRoute.extend({
  addCapability: 'add_procmed',
  additionalModels: [{
    name: 'procmedPricingTypes',
    findArgs: ['lookup', 'procmed_pricing_types']
  }, {
    name: 'physicianList',
    findArgs: ['lookup', 'physician_list']
  }, {
    name: 'tipoProcedimientoList',
    findArgs: ['lookup', 'tipoProcedimientos']
  }],
  allowSearch: false,
  moduleName: 'procmed',
  newButtonText: t('procmed.buttons.newButton'),
  sectionTitle: t('procmed.sectionTitle')
});
