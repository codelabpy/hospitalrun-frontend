import { translationMacro as t } from 'ember-i18n';
import procmedIndexRoute from 'hospitalrun/procmed/index/route';
export default procmedIndexRoute.extend({
  pageTitle: t('procmed.titles.completedProcmed'),
  searchStatus: 'Completado'
});
