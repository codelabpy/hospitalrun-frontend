import AbstractEditController from 'hospitalrun/controllers/abstract-edit-controller';
import Ember from 'ember';

export default AbstractEditController.extend({
  cancelAction: 'closeModal',
  newCharge: false,
  requestingController: Ember.inject.controller('procmed/edit'),
  medicationList: Ember.computed.alias('requestingController.medicationList'),

  updateCapability: 'add_charge',

  title: function() {
    let isNew = this.get('model.isNew');
    if (isNew) {
      return this.get('i18n').t('procmed.titles.addMedicationUsed');
    }
    return this.get('i18n').t('procmed.titles.editMedicationUsed');
  }.property('model.isNew'),

  beforeUpdate() {
    let isNew = this.get('model.isNew');
    let lista = document.getElementById('lista');
    if (isNew) {
      this.set('newCharge', true);
      let model = this.get('model');
      let inventoryItem = model.get('inventoryItem');
      model.set('medication', inventoryItem);
      model.set('medicationTitle', inventoryItem.get('name'));
      model.set('priceOfMedication', inventoryItem.get('price'));
      model.set('medida', lista.value);
    }
    return Ember.RSVP.Promise.resolve();
  },

  afterUpdate(record) {
    console.log('entro en el modal de after');
    if (this.get('newCharge')) {
      this.get('requestingController').send('addCharge', record);
    } else {
      this.send('closeModal');
    }
  }
});
