import AbstractEditController from 'hospitalrun/controllers/abstract-edit-controller';
import ChargeActions from 'hospitalrun/mixins/charge-actions';
import Ember from 'ember';
import PatientSubmodule from 'hospitalrun/mixins/patient-submodule';

export default AbstractEditController.extend(ChargeActions, PatientSubmodule, {
  procmedController: Ember.inject.controller('procmed'),

  chargePricingCategory: 'procmed',
  chargeRoute: 'procmed.charge',
  medicationList: null,
  editController: Ember.inject.controller('visits/edit'),

  canComplete: function() {
    let isNew = this.get('model.isNew');
    let procmedTypeName = this.get('model.procmedTypeName');
    let selectedprocmedType = this.get('selectedprocmedType');
    if (isNew && (Ember.isEmpty(procmedTypeName) || Ember.isArray(selectedprocmedType) && selectedprocmedType.length > 1)) {
      return false;
    } else {
      return this.currentUserCan('complete_procmed');
    }
  }.property('selectedprocmedType.[]', 'model.procmedTypeName'),

  actions: {
    completeprocmed() {
      this.set('model.status', 'Completado');
      this.get('model').validate().then(function() {
        if (this.get('model.isValid')) {
          this.set('model.procmedDate', new Date());
          this.send('update');
        }
      }.bind(this)).catch(Ember.K);
    },

    showAddMedication() {
      let newCharge = this.get('store').createRecord('proc-charge', {
        dateCharged: new Date(),
        newMedicationCharge: true,
        quantity: 1
      });
      this.send('openModal', 'procmed.medication', newCharge);
    },

    showEditMedication(charge) {
      let medicationList = this.get('medicationList');
      let selectedMedication = medicationList.findBy('id', charge.get('medication.id'));
      charge.set('itemName', selectedMedication.name);
      this.send('openModal', 'procmed.medication', charge);
    },

    showDeleteMedication(charge) {
      this.send('openModal', 'dialog', Ember.Object.create({
        closeModalOnConfirm: false,
        confirmAction: 'deleteCharge',
        title: this.get('i18n').t('procmeds.titles.deleteMedicationUsed'),
        message: this.get('i18n').t('procmeds.messages.deleteMedication'),
        chargeToDelete: charge,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));
    },

    /**
     * Save the procmed request(s), creating multiples when user selects multiple procmed tests.
     */
    update() {
      if (this.get('model.isNew')) {
        let newprocmed = this.get('model');
        let selectedprocmedType = this.get('selectedprocmedType');
        if (Ember.isEmpty(this.get('model.status'))) {
          this.set('model.status', 'Solicitado');
        }
        this.set('model.requestedBy', newprocmed.getUserName());
        this.set('model.requestedDate', new Date());

        // Nuevo
        // Si el paciente ya tenia un procedimiento medico
        if (Ember.isEmpty(selectedprocmedType)) {
          this.saveNewPricing(this.get('model.procmedTypeName'), 'procmed', 'model.procmedType').then(function() {
            this.addChildToVisit(newprocmed, 'procmed', 'Procedimiento Medico').then(function() {
              this.saveModel();
            }.bind(this));
          }.bind(this));

        } else {
          this.getSelectedPricing('selectedprocmedType').then(function(pricingRecords) {
            if (Ember.isArray(pricingRecords)) {
              this.createMultipleRequests(pricingRecords, 'procmedType', 'procmed', 'procmed');
            } else {
              this.set('model.procmedType', pricingRecords);
              this.addChildToVisit(newprocmed, 'procmed', 'Procedimiento Medico').then(function() {
                this.saveModel();
              }.bind(this));
            }
          }.bind(this));
        }

        // Edicion
      } else {
        if (Ember.isEmpty(this.get('model.status'))) {
          let newprocmed = this.get('model');
          this.set('model.status', 'Solicitado');
          this.set('model.requestedBy', newprocmed.getUserName());
          this.set('model.requestedDate', new Date());
          this.set('model.procmedDate', new Date());
          let selectedprocmedType = this.get('selectedprocmedType');
          if (Ember.isEmpty(selectedprocmedType)) {
            this.saveNewPricing(this.get('model.procmedTypeName'), 'procmed', 'model.procmedType').then(function() {
              this.addChildToVisit(newprocmed, 'procmed', 'procmed').then(function() {
                this.saveModel();
              }.bind(this));
            }.bind(this));
          } else {
            this.getSelectedPricing('selectedprocmedType').then(function(pricingRecords) {
              if (Ember.isArray(pricingRecords)) {
                this.createMultipleRequests(pricingRecords, 'procmedType', 'procmed', 'procmed');
              } else {
                this.set('model.procmedType', pricingRecords);
                this.addChildToVisit(newprocmed, 'procmed', 'procmed').then(function() {
                  this.saveModel();
                }.bind(this));
              }
            }.bind(this));
          }
        }
        this.saveModel();
      }
    }
  },

  additionalButtons: function() {
    let i18n = this.get('i18n');
    let canComplete = this.get('canComplete');
    let isValid = this.get('model.isValid');
    if (isValid && canComplete) {
      return [{
        buttonAction: 'completeprocmed',
        buttonIcon: 'glyphicon glyphicon-ok',
        class: 'btn btn-primary on-white',
        buttonText: i18n.t('buttons.complete')
      }];
    }
  }.property('canComplete', 'model.isValid'),

  lookupListsToUpdate: [{
    name: 'physicianList',
    property: 'model.medico',
    id: 'physician_list'
  }, {
    name: 'tipoProcedimientoList',
    property: 'model.tipoProcedimiento',
    id: 'tipoProcedimientos'
  }],

  pricingTypeForObjectType: 'procmed Procedure',
  pricingTypes: Ember.computed.alias('procmedController.procmedPricingTypes'),

  pricingList: null, // This gets filled in by the route

  physicianList: Ember.computed.alias('procmedController.physicianList'),
  tipoProcedimientoList: Ember.computed.alias('procmedController.tipoProcedimientoList'),

  updateCapability: 'add_procmed',

  afterUpdate(saveResponse, multipleRecords) {
    let i18n = this.get('i18n');
    this.updateLookupLists();
    let afterDialogAction,
      alertTitle,
      alertMessage;
    if (this.get('model.status') === 'Completado') {
      alertTitle = i18n.t('procmed.alerts.completedTitle');
      alertMessage = i18n.t('procmed.alerts.completedMessage');
    } else {
      alertTitle = i18n.t('procmed.alerts.savedTitle');
      alertMessage = i18n.t('procmed.alerts.savedMessage');
    }
    if (multipleRecords) {
      afterDialogAction = this.get('cancelAction');
    }
    this.saveVisitIfNeeded(alertTitle, alertMessage, afterDialogAction);
    this.set('model.selectPatient', false);
  }

});
