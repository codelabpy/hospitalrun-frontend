import { A, isArray } from '@ember/array';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { all, resolve } from 'rsvp';

import AbstractPagedController from 'hospitalrun/controllers/abstract-paged-controller';
import AddNewPatient from 'hospitalrun/mixins/add-new-patient';
import ChargeActions from 'hospitalrun/mixins/charge-actions';
import DiagnosisActions from 'hospitalrun/mixins/diagnosis-actions';
import FilterList from 'hospitalrun/mixins/filter-list';
import ModalHelper from 'hospitalrun/mixins/modal-helper';
import PatientNotes from 'hospitalrun/mixins/patient-notes';
import PatientSubmodule from 'hospitalrun/mixins/patient-submodule';
import PatientVisits from 'hospitalrun/mixins/patient-visits';
import UserSession from 'hospitalrun/mixins/user-session';
import VisitStatus from 'hospitalrun/utils/visit-statuses';

export default AbstractPagedController.extend(AddNewPatient, ChargeActions, DiagnosisActions, FilterList, ModalHelper, PatientNotes, PatientSubmodule, PatientVisits, UserSession, VisitStatus, {
  additionalModels: [{
    name: 'diagnosisList',
    findArgs: ['lookup', 'diagnosis_list']
  }],
  allowAddDiagnosis: computed.not('model.isNew'),
  diagnosisList: computed.alias('visitsController.diagnosisList'),
  lookupListsToUpdate: [{
    name: 'diagnosisList',
    property: 'model.diagnosis',
    id: 'diagnosis_list'
  }],
  startKey: [],
  updateCapability: 'add_diagnosis',

  actions: {
    confi(appointment) {
      let i18n = this.get('i18n');
      let patientDetails = i18n.t('visits.titles.conficita');
      let confirmMessage = i18n.t('visits.messages.conficita');
      this.displayConfirm(patientDetails, confirmMessage, 'pasamano', appointment);
    },

    pasamano(appointment) {
      if (appointment) {
        let outPatient = false;
        let visitStatus;
        let visitType = appointment.get('appointmentType');

        if (visitType === 'Admission') {
          visitStatus = VisitStatus.ADMITTED;
        } else {
          outPatient = true;
          visitStatus = VisitStatus.CHECKED_IN;
        }

        let newVisit = this.get('store').createRecord('visit');
        newVisit.setProperties({
          outPatient,
          status: visitStatus
        });
        let startDate = new Date();

        let customForms = this.get('customForms');
        let numContacto = appointment.get('location');
        let medico = appointment.get('provider');
        let patient = appointment.get('patient');
        let diagnoses = patient.get('diagnoses');
        let turnos = appointment.get('turnos');
        resolve(appointment);
        newVisit.set('patient', patient);
        newVisit.set('examiner', medico);
        newVisit.set('diagnoses', diagnoses);
        newVisit.set('visitType', visitType);
        newVisit.set('startDate', startDate);
        newVisit.set('customForms', customForms);
        newVisit.set('appointment', appointment);
        newVisit.set('turnos', turnos);
        newVisit.set('location', numContacto);
        newVisit.set('hasAppointment', true);
        newVisit.save();
      }

      let skipAfterUpdate = true;
      appointment.set('status', 'Atendido');
      appointment.save();
      this.saveModel(skipAfterUpdate);
    }
  },

  saveModel(skipAfterUpdate) {
    return get(this, 'model').save().then((record) => {
      this.updateLookupLists().then(() => {
        if (!skipAfterUpdate) {
          this.afterUpdate(record);
        }
      });
    }).catch((error) => {
      this.send('error', error);
    });
  },

  updateLookupLists() {
    let lookupListsToUpdate = get(this, 'lookupListsToUpdate');
    let listsToUpdate = A();
    let lookupPromises = [];

    if (!isEmpty(lookupListsToUpdate)) {
      lookupListsToUpdate.forEach((list) => {
        let propertyValue = get(this, get(list, 'property'));
        let lookupListToUpdate = get(this, get(list, 'name'));

        if (!isEmpty(propertyValue)) {
          if (!isEmpty(lookupListToUpdate) && lookupListToUpdate.then) {
            lookupPromises.push(lookupListToUpdate.then((lookupList) => {
              return this._checkListForUpdate(list, lookupList, listsToUpdate, propertyValue);
            }));
          } else {
            this._checkListForUpdate(list, lookupListToUpdate, lookupListToUpdate, propertyValue);
          }
        }
      });

      if (!isEmpty(lookupPromises)) {
        return all(lookupPromises).then(() => {
          let lookupLists = get(this, 'lookupLists');
          let updatePromises = [];

          listsToUpdate.forEach((list) => {
            updatePromises.push(list.save().then(() => {
              set(this, 'lookupListsLastUpdate', new Date().getTime());
              lookupLists.resetLookupLists(get(list, 'id'));
            }));
          });
          return all(updatePromises);
        });
      }
    }
    return resolve();
  },

  _checkListForUpdate(listInfo, lookupList, listsToUpdate, propertyValue) {
    let store = get(this, 'store');

    if (!lookupList) {
      lookupList = store.push(store.normalize('lookup', { id: listInfo.id, value: [], userCanAdd: true }));
    }
    if (isArray(propertyValue)) {
      propertyValue.forEach(function(value) {
        this._addValueToLookupList(lookupList, value, listsToUpdate, listInfo.name);
      }.bind(this));
    } else {
      this._addValueToLookupList(lookupList, propertyValue, listsToUpdate, listInfo.name);
    }
    return lookupList;
  },

  canAddVisit: function() {
    return this.currentUserCan('add_visit');
  }.property(),

  canEdit: function() {
    // Add and edit are the same capability
    return this.currentUserCan('add_appointment');
  }.property(),

  canDelete: function() {
    return this.currentUserCan('delete_appointment');
  }.property(),

  sortProperties: ['startDate', 'endDate'],
  sortAscending: true
});
