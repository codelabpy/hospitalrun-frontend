import { computed } from '@ember/object';
import AbstractIndexRoute from 'hospitalrun/routes/abstract-index-route';
import moment from 'moment';
import { translationMacro as t } from 'ember-i18n';

export default AbstractIndexRoute.extend({
  editReturn: 'appointments.index',
  addCapability: 'add_visit',
  additionalModels: [{
    name: 'diagnosisList',
    findArgs: ['lookup', 'diagnosis_list']
  }],
  itemsPerPage: null, // Fetch all incidents as one page
  modelName: 'appointment',
  newButtonText: computed('i18n.locale', () => {
    return t('appointments.buttons.newButton');
  }),
  pageTitle: computed('i18n.locale', () => {
    return t('appointments.thisWeek');
  }),

  _getStartKeyFromItem(item) {
    let endDate = item.get('endDate');
    let id = this._getPouchIdFromItem(item);
    let startDate = item.get('startDate');
    if (endDate && endDate !== '') {
      endDate = new Date(endDate);
      if (endDate.getTime) {
        endDate = endDate.getTime();
      }
    }
    if (startDate && startDate !== '') {
      startDate = new Date(startDate);
      if (startDate.getTime) {
        startDate = startDate.getTime();
      }
    }

    return [startDate, endDate, id];
  },

  _modelQueryParams() {
    let endOfWeek = moment().endOf('week').toDate().getTime();
    let startOfWeek = moment().startOf('week').toDate().getTime();
    let maxId = this._getMaxPouchId();
    return {
      options: {
        startkey: [startOfWeek, null, null],
        endkey: [endOfWeek, endOfWeek, maxId]
      },
      mapReduce: 'appointments_by_date' // el mapreduce original era 'appointments_by_lastModified' se debe ver si este hace lo mismo
    };
  },

  actions: {
    editAppointment(appointment) {
      appointment.set('returnTo', this.get('editReturn'));
      this.send('editItem', appointment);
    },

    pasamano(appointment) {
      this.controller.send('pasamano', appointment);
    }
  }
});
