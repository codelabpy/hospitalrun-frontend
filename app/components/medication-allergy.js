import Component from '@ember/component';
import { get, computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';

export default Component.extend({
  classNames: 'ps-info-group long-form',
  store: service(),
  i18n: service(),

  canAddAllergy: null,
  currentAllergy: false,
  displayModal: false,
  editAllergyAction: 'editAllergy',
  patient: null,
  showAddAllergyAction: 'showAddAllergy',

  additionalButtons: computed('currentAllergy', function() {
    let currentAllergy = this.get('currentAllergy');
    let btn = this.get('i18n').t('buttons.delete');
    if (currentAllergy) {
      return [{
        class: 'btn btn-default warning',
        buttonAction: 'deleteAllergy',
        buttonIcon: 'octicon octicon-x',
        buttonText: btn
      }];
    }
  }),

  buttonConfirmText: computed('currentAllergy', function() {
    let i18n = this.get('i18n');
    let currentAllergy = this.get('currentAllergy');
    if (currentAllergy) {
      return i18n.t('buttons.update');
    } else {
      return i18n.t('buttons.add');
    }
  }),

  closeAllergyModal() {
    this.set('currentAllergy', false);
    this.set('displayModal', false);
  },

  modalTitle: computed('currentAllergy', function() {
    let currentAllergy = this.get('currentAllergy');
    let i18n = this.get('i18n');
    if (currentAllergy) {
      return i18n.t('allergies.titles.editAllergy');
    } else {
      return i18n.t('allergies.titles.addAllergy');
    }
  }),

  showAllergies: computed('canAddAllergy', 'patient.allergies.[]', {
    get() {
      let canAddAllergy = get(this, 'canAddAllergy');
      let patientAllergies = get(this, 'patient.allergies');
      return canAddAllergy || !isEmpty(patientAllergies);
    }
  }),

  actions: {
    cancel() {
      this.closeAllergyModal();
    },

    createNewAllergy() {
      this.sendAction('showAddAllergyAction');
    },

    closeModal() {
      this.closeAllergyModal();
    },

    deleteAllergy() {
      let allergy = this.get('currentAllergy');
      let patient = this.get('patient');
      let patientAllergies = patient.get('allergies');
      allergy.destroyRecord().then(() => {
        patientAllergies.removeObject(allergy);
        patient.save().then(() => {
          this.closeAllergyModal();
        });
      });
    },

    editAllergy(allergy) {
      this.sendAction('editAllergyAction', allergy);
    },

    updateAllergy() {
      let model = this.get('patient');
      let allergyModel = this.get('currentAllergy');
      if (!allergyModel) {
        allergyModel = this.get('store').createRecord('allergy', {
          name: this.get('name')
        });
        allergyModel.save().then(() => {
          model.get('allergies').pushObject(allergyModel);
          model.save().then(() => {
            this.set('name', '');
            this.closeAllergyModal();
          });
        });
      } else {
        allergyModel.save().then(() => {
          this.closeAllergyModal();
        });
      }
    }
  }
});
