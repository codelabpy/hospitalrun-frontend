import EmberObject from '@ember/object';
import Component from '@ember/component';
import { alias } from '@ember/object/computed';
import { get } from '@ember/object';
import Ember from 'ember';
import SelectValues from 'hospitalrun/utils/select-values';
import CustomFormManager from 'hospitalrun/mixins/custom-form-manager';

export default Component.extend(SelectValues, CustomFormManager, {
  customForms: Ember.inject.service(),
  formType: null,
  formsForType: null,
  model: null,
  openModalAction: 'openModal',

  formsForSelect: alias('customForms.formsForSelect'),
  formsToDisplay: alias('customForms.formsToDisplay'),
  showAddButton: alias('customForms.showAddButton'),

  didReceiveAttrs() {
    this._super(...arguments);
    this.initFormsForType();
    let customForms = get(this, 'customForms');
    let formType = get(this, 'formType');
    let model = get(this, 'model');
    customForms.setupForms(formType, model);
  },

  actions: {
    addForm() {
      let model = this.get('model');
      let formsForSelect = this.get('formsForSelect');
      this.sendAction('openModalAction', 'custom-form-add', EmberObject.create({
        modelToAddTo: model,
        customForms: formsForSelect
      }));
    }
  }
});
