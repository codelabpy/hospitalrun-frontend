import { get, set } from '@ember/object';
import EmberObject from '@ember/object';
import { inject as service } from '@ember/service';
import { translationMacro as t } from 'ember-i18n';

import AbstractEditRoute from 'hospitalrun/routes/abstract-edit-route';
import AddToPatientRoute from 'hospitalrun/mixins/add-to-patient-route';
import PatientVisits from 'hospitalrun/mixins/patient-visits';

export default AbstractEditRoute.extend(AddToPatientRoute, PatientVisits, {
  customForms: service(),
  modelName: 'report',

  getNewData() {
    let newReportData = {
      reportDate: new Date(),
      customForms: EmberObject.create()
    };
    let customForms = get(this, 'customForms');
    return customForms.setDefaultCustomForms(['opdReport', 'dischargeReport'], newReportData);
  },

  getScreenTitle(model) {
    let state = get(model, 'isNew') ? 'new' : 'edit';
    let type = get(model, 'visit.outPatient') ? 'opd' : 'discharge';
    return t(`reports.${type}.titles.${state}`);
  },

  getDiagnosisContainer(visit) {
    if (get(visit, 'outPatient')) {
      return visit;
    }
    return null;
  },

  getCurrentOperativePlan(patient) {
    let operativePlans = get(patient, 'operativePlans');
    return operativePlans.findBy('isPlanned', true);
  },

  afterModel(model) {
    if (!get(model, 'isNew')) {
      let patient = get(model, 'visit.patient');
      set(model, 'patient', patient);
    }
    if (!get(model, 'visit')) {
      return this.transitionTo('patients');
    }
  },

  setupController(controller, model) {
    this._super(controller, model);
    let visit = get(model, 'visit');
    let patient = get(model, 'patient');
    let isOutPatient = get(visit, 'outPatient');
    set(controller, 'visit', visit);
    set(controller, 'isOutPatient', isOutPatient);
    set(controller, 'diagnosisContainer', this.getDiagnosisContainer(visit));
    set(controller, 'currentOperativePlan', this.getCurrentOperativePlan(patient));
    if (isOutPatient) {
      set(controller, 'nextAppointments', this.getPatientFutureAppointment(visit, true));
    } else {
      set(controller, 'nextAppointment', this.getPatientFutureAppointment(visit));
    }
  }
});