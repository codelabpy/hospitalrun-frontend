import { map, alias, filter } from '@ember/object/computed';
import Controller, { inject as controller } from '@ember/controller';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import FilterList from 'hospitalrun/mixins/filter-list';
import AppointmentStatuses from 'hospitalrun/mixins/appointment-statuses';
import ModalHelper from 'hospitalrun/mixins/modal-helper';
import PatientVisits from 'hospitalrun/mixins/patient-visits';
import SelectValues from 'hospitalrun/utils/select-values';
import UserSession from 'hospitalrun/mixins/user-session';
import VisitStatus from 'hospitalrun/utils/visit-statuses';
import VisitTypes from 'hospitalrun/mixins/visit-types';

export default Controller.extend(AppointmentStatuses, FilterList, ModalHelper, PatientVisits, SelectValues, UserSession, VisitTypes, {
  addPermission: 'add_patient',
  deletePermission: 'delete_patient',
  queryParams: ['visitDate', 'visitLocation', 'turnos', 'startKey', 'startDate'],
  selectedTurns: null,
  startDate: null,
  turns: null,
  visitLocation: null,
  visitDate: null,
  canAddVisit: computed(function() {
    return this.currentUserCan('add_visit');
  }),
  hasAppointmentLabels: computed(function() {
    let i18n = this.get('i18n');
    return [
      i18n.t('visits.labels.haveAppointment'),
      i18n.t('visits.labels.noAppointment')
    ];
  }),
  doneOrdersValues: computed(function() {
    let i18n = this.get('i18n');
    return [
      i18n.t('visits.labels.ordersNotDone'),
      i18n.t('visits.labels.haveDoneOrders')
    ];
  }),
  locationList: map('patientController.locationList.value', SelectValues.selectValuesMap).volatile(),
  patientNames: map('model', function(visit) {
    return visit.get('patient.shortDisplayName');
  }),
  patientController: controller('patients'),
  sexList: alias('patientController.sexList.value'),
  visitTypesList: alias('patientController.visitTypesList'),
  visitTypesValues: map('visitTypes', function(visitType) {
    return visitType.value;
  }),

  checkedInVisits: filter('model.@each.status', function(visit) {
    return visit.get('visitType') !== 'Admission' && visit.get('status') === VisitStatus.CHECKED_IN;
  }),

  filteredVisits: computed('checkedInVisits', 'filterBy', 'filterValue', 'visitLocation', function() {
    let filterBy = this.get('filterBy');
    let filterValue = this.get('filterValue');
    let filteredBy = this.get('filteredBy');
    let visitLocation = this.get('visitLocation');
    let visits = this.get('checkedInVisits');
    if (isEmpty(visitLocation)) {
      filteredBy.delete('location');
    } else {
      filteredBy.set('location', visitLocation);
    }
    return this.filterList(visits, filterBy, filterValue);
  }),

  sortedVisits: computed('filteredVisits', 'sortByKey', 'sortByDesc', function() {
    let filteredList = this.get('filteredVisits');
    return this.sortFilteredList(filteredList);
  }),

  startKey: [],
  actions: {
    checkOut(visit) {
      let i18n = this.get('i18n');
      let patientDetails = { patientName: visit.get('patient.displayName') };
      let confirmMessage =  i18n.t('visits.messages.checkOut', patientDetails);
      this.displayConfirm(i18n.t('visits.titles.checkOut'), confirmMessage,
        'finishCheckOut', visit);
    },

    editVisit(visit) {
      if (this.get('canAddVisit')) {
        visit.set('returnTo', 'patients.outpatient');
        this.transitionToRoute('visits.edit', visit);
      }
    },

    finishCheckOut(visit) {
      this.checkoutVisit(visit, VisitStatus.CHECKED_OUT);
    },

    search() {
      let fieldsToSet = {
        startKey: [],
        previousStartKey: null,
        previousStartKeys: []
      };
      let turns = this.get('model.selectedTurns');
      let visitDate = this.get('model.selectedVisitDate');
      let visitLocation = this.get('model.selectedLocation');
      if (!isEmpty(visitDate)) {
        this.set('visitDate', visitDate.getTime());
      }
      if (isEmpty(visitLocation)) {
        this.set('visitLocation', null);
      } else {
        this.set('visitLocation', visitLocation);
      }
      if (isEmpty(turns)) {
        fieldsToSet.turns = null;
      } else {
        fieldsToSet.turns = turns;
      }
      if (!isEmpty(fieldsToSet)) {
        this.setProperties(fieldsToSet);
      }
    },

    patientCheckIn() {
      this.transitionToRoute('visits.edit', 'checkin').then(function(newRoute) {
        let visitProps = {
          outPatient: true,
          visitType: null,
          returnTo: 'patients.outpatient'
        };
        newRoute.currentModel.setProperties(visitProps);
      });
    }

  }
});
