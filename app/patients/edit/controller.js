import { resolve } from 'rsvp';
import { alias, map } from '@ember/object/computed';
import { inject as controller } from '@ember/controller';
import { inject as service } from '@ember/service';
import EmberObject, {
  get,
  computed,
  setProperties
} from '@ember/object';
import { isEmpty } from '@ember/utils';
import AbstractEditController from 'hospitalrun/controllers/abstract-edit-controller';
import AllergyActions from 'hospitalrun/mixins/allergy-actions';
import BloodTypes from 'hospitalrun/mixins/blood-types';
import DiagnosisActions from 'hospitalrun/mixins/diagnosis-actions';
import PatientId from 'hospitalrun/mixins/patient-id';
import PatientNotes from 'hospitalrun/mixins/patient-notes';
import ReturnTo from 'hospitalrun/mixins/return-to';
import SelectValues from 'hospitalrun/utils/select-values';
import UserSession from 'hospitalrun/mixins/user-session';
import VisitStatus from 'hospitalrun/utils/visit-statuses';
import PatientVisits from 'hospitalrun/mixins/patient-visits';

export default AbstractEditController.extend(AllergyActions, BloodTypes, DiagnosisActions, ReturnTo, UserSession, PatientId, PatientNotes, PatientVisits, {

  canAddAppointment: function() {
    return this.currentUserCan('add_appointment');
  }.property(),

  canAddContact: function() {
    return this.currentUserCan('add_patient');
  }.property(),

  canAddImaging: function() {
    return this.currentUserCan('add_imaging');
  }.property(),

  canAddLab: function() {
    return this.currentUserCan('add_lab');
  }.property(),

  canAddMedication: function() {
    return this.currentUserCan('add_medication');
  }.property(),

  canAddPhoto: function() {
    let isFileSystemEnabled = this.get('isFileSystemEnabled');
    return (this.currentUserCan('add_photo') && isFileSystemEnabled);
  }.property(),

  canAddProcmed: function() {
    return this.currentUserCan('add_procmed');
  }.property(),

  canAddSocialWork: function() {
    return this.currentUserCan('add_socialwork');
  }.property(),

  canAddVisit: function() {
    return this.currentUserCan('add_visit');
  }.property(),

  canDeleteAppointment: function() {
    return this.currentUserCan('delete_appointment');
  }.property(),

  canDeleteContact: function() {
    return this.currentUserCan('add_patient');
  }.property(),

  canDeleteImaging: function() {
    return this.currentUserCan('delete_imaging');
  }.property(),

  canDeleteLab: function() {
    return this.currentUserCan('delete_lab');
  }.property(),

  canDeleteMedication: function() {
    return this.currentUserCan('delete_medication');
  }.property(),

  canDeletePhoto: function() {
    return this.currentUserCan('delete_photo');
  }.property(),

  canDeleteProcmed: function() {
    return this.currentUserCan('delete_procmed');
  }.property(),

  canDeleteSocialWork: function() {
    return this.currentUserCan('delete_socialwork');
  }.property(),

  canDeleteVisit: function() {
    return this.currentUserCan('delete_visit');
  }.property(),

  patientTypes: computed(function() {
    let i18n = get(this, 'i18n');
    let types = [
      'Socio', // 'Charity',
      'NoSocio' // 'Private'
    ];
    return types.map((type) => {
      return i18n.t(`patients.labels.patientType${type}`);
    });
  }),

  config: service(),
  filesystem: service(),
  database: service(),
  patientController: controller('patients'),

  addressOptions: alias('patientController.addressOptions'),
  address1Include: alias('patientController.addressOptions.value.address1Include'),
  address1Label: alias('patientController.addressOptions.value.address1Label'),
  address2Include: alias('patientController.addressOptions.value.address2Include'),
  address2Label: alias('patientController.addressOptions.value.address2Label'),
  address3Include: alias('patientController.addressOptions.value.address3Include'),
  address3Label: alias('patientController.addressOptions.value.address3Label'),
  address4Include: alias('patientController.addressOptions.value.address4Include'),
  address4Label: alias('patientController.addressOptions.value.address4Label'),

  clinicList: alias('patientController.clinicList'),
  countryList: alias('patientController.countryList'),
  diagnosisList: alias('patientController.diagnosisList'),
  isFileSystemEnabled: alias('filesystem.isFileSystemEnabled'),
  pricingProfiles: map('patientController.pricingProfiles', SelectValues.selectObjectMap),
  sexList: alias('patientController.sexList'),
  statusList: alias('patientController.statusList'),

  haveAdditionalContacts: function() {
    let additionalContacts = this.get('model.additionalContacts');
    return !isEmpty(additionalContacts);
  }.property('model.additionalContacts'),

  haveAddressOptions: function() {
    let addressOptions = this.get('addressOptions');
    return !isEmpty(addressOptions);
  }.property('addressOptions'),

  /* haveCedula: function() {
      let cedula = this.get('model.cedula');
      if (cedula == "0") {
        while (cedula != null && cedula != "") {
          return true;
        }
      }
      else {
        return false;
      }
    }.property('model.cedula')
  */

  lookupListsToUpdate: [{
    name: 'countryList',
    property: 'model.country',
    id: 'country_list'
  }, {
    name: 'clinicList',
    property: 'model.clinic',
    id: 'clinic_list'
  }, {
    name: 'sexList',
    property: 'model.sex',
    id: 'sex'
  }, {
    name: 'statusList',
    property: 'model.status',
    id: 'patient_status_list'
  }],

  patientImaging: function() {
    return this.getVisitCollection('imaging');
  }.property('model.visits.[].imaging'),

  patientLabs: function() {
    return this.getVisitCollection('labs');
  }.property('model.visits.[].labs'),

  patientMedications: function() {
    return this.getVisitCollection('medication');
  }.property('model.visits.[].medication'),

  patientProcedures: function() {
    let visits = this.get('model.visits');
    let operationReports = get(this, 'model.operationReports');
    return this._getPatientProcedures(operationReports, visits);
  }.property('model.visits.[].procedures', 'model.operationReports.[].procedures'),

  patientProcmed: function() {
    return this.getVisitCollection('procmed');
  }.property('model.visits.[].procmed'),

  showExpenseTotal: function() {
    let expenses = this.get('model.expenses');
    return !isEmpty(expenses);
  }.property('model.expenses.[]'),

  totalExpenses: function() {
    let expenses = this.get('model.expenses');
    if (!isEmpty(expenses)) {
      let total = expenses.reduce(function(previousValue, expense) {
        if (!isEmpty(expense.cost)) {
          return previousValue + parseInt(expense.cost);
        }
      }, 0);
      return total;
    }
  }.property('model.expenses.@each.cost'),

  updateCapability: 'add_patient',

  actions: {
    addAllergy(newAllergy) {
      let patient = get(this, 'model');
      this.savePatientAllergy(patient, newAllergy);
    },

    addContact(newContact) {
      let additionalContacts = this.getWithDefault('model.additionalContacts', []);
      let model = this.get('model');
      additionalContacts.addObject(newContact);
      model.set('additionalContacts', additionalContacts);
      this.send('update', true);
      this.send('closeModal');
    },

    addDiagnosis(newDiagnosis) {
      let diagnoses = this.get('model.diagnoses');
      diagnoses.addObject(newDiagnosis);
      this.send('update', true);
      this.send('closeModal');
    },

    returnToPatient() {
      this.transitionToRoute('patients.index');
    },
    /**
     * Add the specified photo to the patient's record.
     */
    addPhoto(savedPhotoRecord) {
      let photos = this.get('model.photos');
      photos.addObject(savedPhotoRecord);
      this.send('closeModal');
    },

    appointmentDeleted(deletedAppointment) {
      let appointments = this.get('model.appointments');
      appointments.removeObject(deletedAppointment);
      this.send('closeModal');
    },

    deleteAllergy(allergy) {
      let patient = get(this, 'model');
      this.deletePatientAllergy(patient, allergy);
    },

    deleteContact(model) {
      let contact = model.get('contactToDelete');
      let additionalContacts = this.get('model.additionalContacts');
      additionalContacts.removeObject(contact);
      this.send('update', true);
    },

    deleteExpense(model) {
      let expense = model.get('expenseToDelete');
      let expenses = this.get('model.expenses');
      expenses.removeObject(expense);
      this.send('update', true);
    },

    deleteFamily(model) {
      let family = model.get('familyToDelete');
      let familyInfo = this.get('model.familyInfo');
      familyInfo.removeObject(family);
      this.send('update', true);
    },

    deletePhoto(model) {
      let photo = model.get('photoToDelete');
      let photoId = photo.get('id');
      let photos = this.get('model.photos');
      let filePath = photo.get('fileName');
      photos.removeObject(photo);
      photo.destroyRecord().then(function() {
        let fileSystem = this.get('filesystem');
        let isFileSystemEnabled = this.get('isFileSystemEnabled');
        if (isFileSystemEnabled) {
          let pouchDbId = this.get('database').getPouchId(photoId, 'photo');
          fileSystem.deleteFile(filePath, pouchDbId);
        }
      }.bind(this));
    },

    editAppointment(appointment) {
      if (this.get('canAddAppointment')) {
        appointment.set('returnToPatient', this.get('model.id'));
        appointment.set('returnTo', null);
        this.transitionToRoute('appointments.edit', appointment);
      }
    },

    editImaging(imaging) {
      if (this.get('canAddImaging')) {
        if (imaging.get('canEdit')) {
          imaging.set('returnToPatient', this.get('model.id'));
          this.transitionToRoute('imaging.edit', imaging);
        }
      }
    },

    editLab(lab) {
      if (this.get('canAddLab')) {
        if (lab.get('canEdit')) {
          lab.setProperties('returnToPatient', this.get('model.id'));
          this.transitionToRoute('labs.edit', lab);
        }
      }
    },

    editMedication(medication) {
      if (this.get('canAddMedication')) {
        if (medication.get('canEdit')) {
          medication.set('returnToPatient', this.get('model.id'));
          this.transitionToRoute('medication.edit', medication);
        }
      }
    },

    editOperativePlan(operativePlan) {
      let model = operativePlan;
      if (isEmpty(model)) {
        this._addChildObject('patients.operative-plan', (route) =>{
          route.controller.getPatientDiagnoses(this.get('model'), route.currentModel);
        });
      } else {
        model.set('returnToVisit');
        model.set('returnToPatient', this.get('model.id'));
        this.transitionToRoute('patients.operative-plan', model);
      }
    },

    editOperationReport(operationReport) {
      operationReport.set('returnToPatient', this.get('model.id'));
      this.transitionToRoute('patients.operation-report', operationReport);
    },

    editPhoto(photo) {
      this.send('openModal', 'patients.photo', photo);
    },

    editProcedure(procedure) {
      if (this.get('canAddVisit')) {
        procedure.set('patient', this.get('model'));
        procedure.set('returnToVisit');
        procedure.set('returnToPatient', this.get('model.id'));
        this.transitionToRoute('procedures.edit', procedure);
      }
    },

    editProcmed(procmed) {
      if (this.get('canAddProcmed')) {
        if (procmed.get('canEdit')) {
          procmed.set('returnToPatient', this.get('model.id'));
          this.transitionToRoute('procmed.edit', procmed);
        }
      }
    },

    editVisit(visit) {
      if (this.get('canAddVisit')) {
        visit.set('returnToPatient', this.get('model.id'));
        this.transitionToRoute('visits.edit', visit);
      }
    },

    importAction(cedula) {
      console.log(cedula);
      let model = this.get('model');
      let self = this;
      model.set('firstName', null);
      model.set('externalPatientId', null);
      model.set('lastName', null);
      model.set('sex', null);
      model.set('dateOfBirth', null);
      model.set('occupation', null);
      model.set('phone', null);
      model.set('email', null);
      model.set('address', null);
      model.set('country', null);
      model.set('age', null);
      model.set('patientType', null);
      model.set('bloodType', null);
      model.set('placeOfBirth', null);
      // model.set('parent', null);
      // model.set('parentCi', null);
      $.getJSON(`https://salud.sancristobal.coop.py/personas/cpcRest-RESTWebService-context-root/rest/v0/persona/${cedula}`, {}).done(function(json) {
        model.set('firstName', json.SocPerNombres);
        model.set('externalPatientId', json.SocPerNroSocio);
        model.set('lastName', json.SocPerApellidos);
        model.set('sex', json.SocPerSexo);
        model.set('dateOfBirth', new Date(json.SocPerFecNacim.replace(/-/g, '/')));
        model.set('phone', json.SocPerTelefono);
        model.set('email', json.SocPerEmail);
        model.set('address', json.SocPerDireccion);
        model.set('country', json.nacionalidad);
        model.set('occupation', json.ocupacion);
      }).fail(function(error) {
        console.log(error);
        if (error.status == 404) {
          self.displayAlert('Persona no registrada', 'Nro de cedula no registrada. Debe cargarlo manualmente.');
        } else {
          self.displayAlert('Error de conexión', 'Error en la conexion a la consulta de socios. Consulte con su servicio técnico');
        }
      });
      model.set('age');
      this.transitionToRoute('patients.edit');
    },

    importParentAction(cedula) {
      let model = this.get('model');
      model.set('parent', null);
      let self = this;
      $.getJSON(`https://salud.sancristobal.coop.py/personas/cpcRest-RESTWebService-context-root/rest/v0/persona/${cedula}`, {}).done(function(json) {
        model.set('parent', `${json.SocPerNombres} ${json.SocPerApellidos}`);
      }).fail(function(error) {
        console.log(error);
        if (error.status == 404) {
          self.displayAlert('Persona no registrada', 'Nro de cedula no registrada. Debe cargarlo manualmente.');
        } else {
          self.displayAlert('Error de conexión', 'Error en la conexion a la consulta de socios. Consulte con su servicio técnico');
        }
      });
      this.transitionToRoute('patients.edit');
    },

    newAppointment() {
      this._addChildObject('appointments.edit');
    },

    newImaging() {
      this._addChildObject('imaging.edit');
    },

    newLab() {
      this._addChildObject('labs.edit');
    },

    newMedication() {
      this._addChildObject('medication.edit');
    },

    newProcmed() {
      this._addChildObject('procmed.edit');
    },

    newSurgicalAppointment() {
      this.transitionToRoute('appointments.edit', 'newsurgery').then((newRoute) => {
        newRoute.currentModel.setProperties({
          patient: this.get('model'),
          returnToPatient: this.get('model.id'),
          selectPatient: false
        });
      });
    },

    newVisit() {
      let patient = this.get('model');
      this.send('createNewVisit', patient, true);
    },

    showAddContact() {
      this.send('openModal', 'patients.add-contact', {});
    },

    showAddPhoto() {
      let newPatientPhoto = this.get('store').createRecord('photo', {
        patient: this.get('model'),
        saveToDir: `${this.get('model.id')}/photos/`
      });
      newPatientPhoto.set('editController', this);
      this.send('openModal', 'patients.photo', newPatientPhoto);
    },

    showAddPatientNote(model) {
      if (this.get('canAddNote')) {
        if (isEmpty(model)) {
          model = this.get('store').createRecord('patient-note', {
            patient: this.get('model'),
            createdBy: this.getUserName()
          });
        }
        this.send('openModal', 'patients.notes', model);
      }
    },

    showDeleteAppointment(appointment) {
      appointment.set('deleteFromPatient', true);
      this.send('openModal', 'appointments.delete', appointment);
    },

    showDeleteContact(contact) {
      this.send('openModal', 'dialog', EmberObject.create({
        confirmAction: 'deleteContact',
        title: this.get('i18n').t('patients.titles.deleteContact'),
        message: this.get('i18n').t('patients.titles.deletePhoto', { object: 'contact' }),
        contactToDelete: contact,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));
    },

    showDeleteExpense(expense) {
      this.send('openModal', 'dialog', EmberObject.create({
        confirmAction: 'deleteExpense',
        title: this.get('i18n').t('patients.titles.deleteExpense'),
        message: this.get('i18n').t('patients.titles.deletePhoto', { object: 'expense' }),
        expenseToDelete: expense,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));
    },

    showDeleteFamily(familyInfo) {
      this.send('openModal', 'dialog', EmberObject.create({
        confirmAction: 'deleteFamily',
        title: this.get('i18n').t('patients.titles.deleteFamilyMember'),
        message: this.get('i18n').t('patients.titles.deletePhoto', { object: 'family member' }),
        familyToDelete: familyInfo,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));

    },

    showDeleteImaging(imaging) {
      this.send('openModal', 'imaging.delete', imaging);
    },

    showDeleteLab(lab) {
      this.send('openModal', 'labs.delete', lab);
    },

    showDeleteMedication(medication) {
      this.send('openModal', 'medication.delete', medication);
    },

    showDeletePhoto(photo) {
      this.send('openModal', 'dialog', EmberObject.create({
        confirmAction: 'deletePhoto',
        title: this.get('i18n').t('patients.titles.deletePhoto'),
        message: this.get('i18n').t('patients.titles.deletePhoto', { object: 'photo' }),
        photoToDelete: photo,
        updateButtonAction: 'confirm',
        updateButtonText: this.get('i18n').t('buttons.ok')
      }));
    },

    showDeleteProcmed(procmed) {
      this.send('openModal', 'procmed.delete', procmed);
    },

    showDeleteVisit(visit) {
      visit.set('deleteFromPatient', true);
      this.send('openModal', 'visits.delete', visit);
    },

    showEditExpense(expenseInfo) {
      this._showEditSocial(expenseInfo, 'social-expense', 'expense');
    },

    showEditFamily(familyInfo) {
      this._showEditSocial(familyInfo, 'family-info', 'family-info');
    },

    updateExpense(model) {
      this._updateSocialRecord(model, 'expenses');
    },

    updateFamilyInfo(model) {
      this._updateSocialRecord(model, 'familyInfo');
    },

    visitDeleted(deletedVisit) {
      let visits = this.get('model.visits');
      let patient = this.get('model');
      let patientCheckedIn = patient.get('checkedIn');
      let patientAdmitted = patient.get('admitted');
      visits.removeObject(deletedVisit);
      if (patientAdmitted || patientCheckedIn) {
        let patientUpdate = false;
        if (patientAdmitted && isEmpty(visits.findBy('status', VisitStatus.ADMITTED))) {
          patient.set('admitted', false);
          patientUpdate = true;
        }
        if (patientCheckedIn && isEmpty(visits.findBy('status', VisitStatus.CHECKED_IN))) {
          patient.set('checkedIn', false);
          patientUpdate = true;
        }
        if (patientUpdate === true) {
          patient.save().then(() => this.send('closeModal'));
        } else {
          this.send('closeModal');
        }
      } else {
        this.send('closeModal');
      }
    }

  },

  _addChildObject(route, afterTransition) {
    let options = {
      queryParams: {
        forPatientId: this.get('model.id')
      }
    };
    this.transitionToRoute(route, 'new', options).then((newRoute) => {
      if (afterTransition) {
        afterTransition(newRoute);
      }
    });
  },

  _showEditSocial(editAttributes, modelName, route) {
    let model;
    if (isEmpty(editAttributes)) {
      model = this.get('store').createRecord(modelName, {
        newRecord: true
      });
    } else {
      model = this.get('store').push({
        data: {
          id: get(editAttributes, 'id'),
          type: modelName,
          attributes: editAttributes
        }
      });
    }
    this.send('openModal', `patients.socialwork.${route}`, model);
  },

  getVisitCollection(name) {
    let visits = this.get('model.visits');
    return this._getVisitCollection(visits, name);
  },

  _updateSocialRecord(recordToUpdate, name) {
    let socialRecords = this.getWithDefault(`model.${name}`, []);
    let isNew = recordToUpdate.get('isNew');
    let patient = this.get('model');
    let objectToUpdate = recordToUpdate.serialize();
    objectToUpdate.id = recordToUpdate.get('id');
    if (isNew) {
      socialRecords.addObject(EmberObject.create(objectToUpdate));
    } else {
      let updateRecord = socialRecords.findBy('id', objectToUpdate.id);
      setProperties(updateRecord, objectToUpdate);
    }
    patient.set(name, socialRecords);
    this.send('update', true);
    this.send('closeModal');
  },

  _updateSequence(record) {
    let config = this.get('config');
    let friendlyId = record.get('friendlyId');
    return config.getPatientPrefix().then((prefix) => {
      let re = new RegExp(`^${prefix}\\d{5}$`);
      if (!re.test(friendlyId)) {
        return;
      }
      return this.store.find('sequence', 'patient').then((sequence) => {
        let sequenceNumber = sequence.get('value');
        let patientNumber = parseInt(friendlyId.slice(prefix.length));
        if (patientNumber > sequenceNumber) {
          sequence.set('value', patientNumber);
          return sequence.save();
        }
      });
    });
  },

  verifyDocumentNumberDuplicity(cedula) {
    let model = this.get('model');
    model.set('parent', null);
    let self = this;
    let [patientList] = self.patientList;

    for (let index = 0; index < patientList.length; index++) {
      if (patientList[index].cedula == cedula) {
        return true;
      }
    }
    return false;
  },

  beforeUpdate() {
    // Se obtienen los datos antes del contexto de la llamada ajax
    let cedula = this.model.get('cedula');
    let lastName = this.model.get('lastName');
    let firstName = this.model.get('firstName');
    let nacionalidad = this.model.get('country');
    if (cedula != 0 && nacionalidad == 'PARAGUAYA') {
      $.getJSON(`https://salud.sancristobal.coop.py/personas/cpcRest-RESTWebService-context-root/rest/v0/persona/${cedula}`, {}).done(function() {
        console.log('actualizar');
      }).fail(function(error) {
        if (error.status == 404) {
          let data = {
            'SocPerCedula': cedula,
            'SocPerApellidos': lastName,
            'SocPerNombres': firstName,
            'SocPerBarrio': 1,
            'SocPerDirModif': 'N',
            'SocPerEstSocio': 'N',
            'SocPerGradoInst': 12,
            'SocPerProfesion': 9999,
            'SocPerDireccion': 'DESCONOCIDO',
            'SocPerEstCivil': 'OTROS',
            'SocPerFecNacim': '1984-01-01',
            'nacionalidad': 'PARAGUAYA',
            'ocupacion': 'OTROS',
            'SocPerSexo': 'FEMENINO'
          };

          $.ajax({
            type: 'POST',
            url: 'https://salud.sancristobal.coop.py/personas/cpcRest-RESTWebService-context-root/rest/v0/persona/',
            dataType: 'json',
            contentType: 'application/vnd.oracle.adf.resourceitemjson',
            data: JSON.stringify(data),
            success() {
              self.displayAlert('Nueva persona registrada', 'Se ha incluido una nueva persona');
            },
            error() {
              self.displayAlert('Error al crear una persona', 'No se ha podido insertar la persona. Contacte con su soporte tecnico');
            }
          });
        }
      });
    }

    if (this.get('model.isNew')) {
      if (this.verifyDocumentNumberDuplicity(cedula)) {
        let err = {
          message: 'El número de cedula proporcionado ya existe en el sistema. Por favor verifíquelo.'
        };
        throw err;
      }
    }

    if (!this.get('model.isNew')) {
      let socio = this.model.get('externalPatientId');
      if (socio != null && socio != '') {
        this.model.set('patientType', 'Socio');
      } else if (socio == null || socio == '') {
        this.model.set('patientType', 'No Socio');
      }
      return resolve();
    }
    let socio = this.model.get('externalPatientId');
    if (socio != null || socio != '') {
      this.model.set('patientType', 'Socio');
    }
    if (socio == null || socio == '') {
      this.model.set('patientType', 'No Socio');
    }
    if (!this.get('model.isNew')) {
      return resolve();
    }
    return this.generateFriendlyId('patient').then((friendlyId) => {
      this.model.set('friendlyId', friendlyId);
    });
  },

  afterUpdate(record) {
    this._updateSequence(record).then(() => {
      $('.message').show();
      $('.message').text(this.get('i18n').t('patients.messages.savedPatient', record));
      $('.message').delay(3000).fadeOut(100);
    });
  }

});
