import Service, { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import EmberObject, { computed, get, set } from '@ember/object';

export default Service.extend({
  store: service(),

  formsForSelect: computed('formsForType', 'usedForms', function() {
    let formsForType = get(this, 'formsForType');
    let usedForms = get(this, 'usedForms');
    if (!isEmpty(formsForType)) {
      let formsForSelect = formsForType.filter((customForm) => {
        return (!usedForms.includes(get(customForm, 'id')));
      });
      formsForSelect = formsForSelect.map((customForm) => {
        return {
          id: get(customForm, 'id'),
          value: get(customForm, 'name')
        };
      });
      return formsForSelect;
    }
  }),

  formsToDisplay: computed('formsForType', 'currentModel.customForms', function() {
    let formsForType = get(this, 'formsForType');
    let modelForms = get(this, 'currentModel.customForms');
    if (!isEmpty(modelForms) && !isEmpty(formsForType)) {
      return Object.keys(modelForms).map((formId) => {
        return {
          form: formsForType.findBy('id', formId),
          propertyPrefix: `customForms.${formId}.`
        };
      });
    }
  }),

  showAddButton: computed('formsForSelect', function() {
    let formsForSelect = get(this, 'formsForSelect');
    return !isEmpty(formsForSelect);
  }),

  usedForms: computed('currentModel.customForms', function() {
    let modelForms = get(this, 'currentModel.customForms');
    if (isEmpty(modelForms)) {
      return [];
    } else {
      return Object.keys(modelForms);
    }
  }),

  getCustomForms(formTypes) {
    return this.get('store').query('custom-form', {
      options: {
        keys: formTypes
      },
      mapReduce: 'custom_form_by_type'
    });
  },

  resetCustomFormByType(formType) {
    let customForms = get(this, 'customForms');
    delete customForms[formType];
  },

  setDefaultCustomForms(customFormNames, model) {
    return this.getCustomForms(customFormNames).then((customForms) => {
      if (!isEmpty(customForms)) {
        customForms.forEach((customForm) => {
          if (get(customForm, 'alwaysInclude')) {
            set(model, `customForms.${get(customForm, 'id')}`, EmberObject.create());
          }
        });
      }
      return model;
    });
  },

  setupForms(formType, model) {
    this.getCustomForms([formType]).then((forms) => {
      set(this, 'currentModel', model);
      set(this, 'formsForType', forms);
    });
  },

  _getCustomFormsFromCache(formTypes) {
    let customForms = get(this, 'customForms');
    let returnForms = [];
    formTypes.forEach((formType) => {
      returnForms.addObjects(customForms[formType]);
    });
    return returnForms;
  }
});
